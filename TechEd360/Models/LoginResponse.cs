﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{


    public class Response {
     public bool success { get; set; }
     public string[] message;
    }

   public class LoginResponse : Response
    {
       
        public string auth_token { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
    }
}
