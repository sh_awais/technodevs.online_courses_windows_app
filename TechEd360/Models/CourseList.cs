﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechEd360;

namespace Models
{

  public  class CourseList
    {
        public int total { get; set; }
        public IList<Cours> courses { get; set; }
    }

  public class Cours
  {
      
        public int id { get; set; }
        public string title { get; set; }
        public string category { get; set; }
        public string banner_image { get; set; }
        public string image_url { get; set; }

        // /course
        public string download_all { get; set; }
        public IList<int> available_course_files_ids { get; set; }

        //{courseId}/download_course
        public int course_files_count { get; set; }
        public IList<CoureFile> course_files { get; set; }
  }

  public class CoureFile
  {
      public CoureFile()
      {
          download_status = DownloadStatus.NotStarted;
      }

      public int id { get; set; }
      public string file_name { get; set; }
      //public string file_path { get; set; }
      public string content_type { get; set; }
      public int download_count { get; set; }
      public string download_url { get; set; }
      //public string local_url { get; set; }
      public DownloadStatus download_status { get; set; }

        //these properties are added manually from course
        public string courseTitle { get; set; }
        public string categoryTitle { get; set; }

        public string GetFolderPath()
        {
            string filePath = UserData.Default.email;
            filePath += "\\" + categoryTitle;
            filePath += "\\" + courseTitle;
            filePath += "\\" + GetFolderName(content_type);
            return filePath;
        }

        public string GetLocalPath() {
           
            return GetFolderPath() + "\\" + file_name;
        }

        public string GetTempPath()
        {
           
            return  GetLocalPath() + ".tmp";
        }

        public  string GetFolderName(string content_type)
        {

            content_type = content_type.Substring(0, content_type.IndexOf("/"));
            switch (content_type)
            {
                case "video":
                    return "Video";

                case "audio":
                    return "Audio";
                default:
                    return "Ebook";

            }

        }

    }
    
       
   public  enum DownloadStatus
    {
        Downloading = 0,
        Downloaded = 1,
        NotStarted = 2    
    }

    }


