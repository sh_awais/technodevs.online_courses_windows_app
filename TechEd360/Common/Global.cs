﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using TechEd360.UserControls;

namespace TechEd360
{
    public   class  Global
    {

     // public static Control PreviousUc { get; set; }
        public static int SelectedCourse { get; set; }  //for current course selected
        public static string courseListFile
        {
            get
            {
                return "courseList.json";
            }
        }  //for course file name
        public static bool isServerStarted = false;

        public static BitmapImage getBitmapImage(string url)
        {
         
                string fileName = Path.GetFileName(url.Replace(@"+", string.Empty));
                int index = fileName.IndexOf("?");
                if (index > 0)
                    fileName = fileName.Substring(0, index);

                // Create a BitmapSource  
                var path = Path.Combine(Environment.CurrentDirectory,"images", fileName);
                var uri = new Uri(path);
                return new BitmapImage(uri);
        
        } 
        
        // return image from path
        public static string PadZero(int n) {

            var iString = n.ToString();

            if (iString.Length == 1)
            {
                iString = iString.PadLeft(2, '0') + '.'; //RIGHT HERE!!!
            }

            return iString;
        }

        public static CourseList courseList = new CourseList();

        public static void Navigate(UserControl uc, int CoursePosition) {
            var main = App.Current.MainWindow as MainWindow;
           SelectedCourse = CoursePosition;
            var pGird = main.ParentGrid.Children[0] as UcMainMenu;
            pGird.GridMain.Navigate(uc);
        }


        // For Navigation To Next Screen 
        public static void Navigate(UserControl uc)
        {
            var main = App.Current.MainWindow as MainWindow;
            var pGird = main.ParentGrid.Children[0] as UcMainMenu;
            pGird.GridMain.Content= uc;
            Module.NavigationList.Add(uc);

        }

        // Send Current Files
        public static List<CoureFile> GetCurrentCourseFiles() {
        
            if (courseList.courses[SelectedCourse].course_files !=null) {
               return courseList.courses[SelectedCourse].course_files.ToList();
            }
            return null;
            
        }

        //Send Course Title

        public static string GetCurrentCourseTitle() {
            return courseList.courses[SelectedCourse].title;
        }

        public static BitmapImage  GetCurrentCourseImage()
        {
            return getBitmapImage(courseList.courses[SelectedCourse].image_url);
        }

        public static string GetFolderName(string content_type)
        {

            content_type = content_type.Substring(0, content_type.IndexOf("/"));
            switch (content_type)
            {

                case "video":
                    return "Video";

                case "audio":
                    return "Audio";

                default:
                    return "Ebook";



            }

        }


        //Clear Folder

        public static void clearFolder(string FolderName)
        {
            if (Directory.Exists(FolderName)) {

            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                clearFolder(di.FullName);
                di.Delete();
            }

            }
        }

        public static string GetUploadPath()
        {

            string filePath;
            filePath = Directory.GetCurrentDirectory().ToString();
            filePath += "\\" + UserData.Default.email + "\\Uploads\\Uploads";


            return filePath;

        }

        public static string GetDirectoryName(string fileExtention) {

            string dir = "";

            switch (fileExtention) {

                case ".mp4":

                    dir = "Video";
                    break;

                case ".mp3":

                    dir = "Audio";
                    break;

                default:

                    dir = "Ebook";
                    break;
            }

            return dir;
        }

    }

}
