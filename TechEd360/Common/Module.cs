﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Microsoft.VisualBasic;
using TechEd360;
using TechEd360.UserControls;

static class Module
{

    public static MainWindow Mian = App.Current.MainWindow as MainWindow;
   
    // BreadCrumbs Settings (Display_Name | Page_Name)
    public static ArrayList listBreadCrumbs = new ArrayList();

    public static List<UserControl> NavigationList = new List<UserControl>();

    //public static string bcHome = "HOME|Home";
    //public static string bcItem1 = "ITEM 1|Item1";
    //public static string bcItem2 = "ITEM 2|Item2";
    //public static string bcSubItem1_1 = "SUBITEM 1.1|SubItem1_1";
    //public static string bcSubItem1_2 = "SUBITEM 1.2|SubItem1_2";
    //public static string bcSubItem2_1 = "SUBITEM 2.1|SubItem2_1";
    //public static string bcSubItem2_2 = "SUBITEM 2.2|SubItem2_2";



    // Bind Bread Crumbs
    public static void BindBreadCrumbs()
    {
        TextBlock tbMain = new TextBlock();

        int CntBreadCrumbs = 0;
        var bc = new BrushConverter();

        foreach (string l in listBreadCrumbs)
        {
            TextBlock tb = new TextBlock();
            
          //  string[] listNameText = l.Split('|'); 
            tb.Text = l;
            //tb.Name = listNameText[1];
            tb.FontWeight = FontWeights.Regular;
            tb.FontSize = 12;
            tb.FontFamily = new FontFamily("file:///Fonts/Roboto-Regular");
            tb.Foreground = (Brush)bc.ConvertFrom("#ec4a24");

            if (CntBreadCrumbs != listBreadCrumbs.Count - 1)
            {
                tb.Foreground = (Brush)bc.ConvertFrom("#313131");
                tb.Cursor = Cursors.Hand;
                tb.PreviewMouseDown += tbBreadCrumbsClick;
                tbMain.Inlines.Add(tb);
               

                TextBlock tbArrow = new TextBlock();
                tbArrow.Text = "  /  ";
                tbArrow.FontWeight = FontWeights.Bold;
                tbArrow.Margin = new Thickness(0, -2, 0, 0);
                tbMain.Inlines.Add(tbArrow);
            }
            else
                // tb.Foreground = DirectCast(bc.ConvertFrom("#009F26"), Brush)
               
            tbMain.Inlines.Add(tb);
            CntBreadCrumbs += 1;
        }

        var MainMenu = Mian.ParentGrid.Children[0] as UcMainMenu;
        MainMenu.lblBreadCrumb.Content= tbMain;
        // Mian.lblBreadCrumb.Content = tbMain;
    }

    // Remove Bread Crumbs List On Bread Crumbs Click
    public static void tbBreadCrumbsClick(object sender, MouseButtonEventArgs e)
    {
        TextBlock tb = (TextBlock)sender;
        string bcClick = tb.Text;
        // + "|" + tb.Name;
        //int intStart = listBreadCrumbs.IndexOf(bcClick) + 1;
        //int intEnd = listBreadCrumbs.Count - intStart;

        int intStart = listBreadCrumbs.IndexOf(bcClick)+1;
        int intEnd = listBreadCrumbs.Count - intStart;
        listBreadCrumbs.RemoveRange(intStart, intEnd);




        // UI Update
        var selectedIndex = listBreadCrumbs.IndexOf(bcClick);
        var main = App.Current.MainWindow as MainWindow;
           var pGird = main.ParentGrid.Children[0] as UcMainMenu;
        UserControl Uc = NavigationList[selectedIndex];
        pGird.GridMain.Content = Uc;

        List<UserControl> NewNavigationList = new List<UserControl>();
        for (int i = 0; i <= selectedIndex; i++) {

            NewNavigationList.Add(NavigationList[i]);
        }

        NavigationList = NewNavigationList;

   



        // Load UserControl from String Name

        //int diff = intEnd - intStart;

        //for (int i = 0; i <= diff;i++) {
        //    var main = App.Current.MainWindow as MainWindow;
        //    var pGird = main.ParentGrid.Children[0] as UcMainMenu;
        //    if (pGird.GridMain.CanGoBack)
        //    {
        //        pGird.GridMain.NavigationService.GoBack();
        //    }

        //    //  UserControl UC = Application.LoadComponent(new Uri("UserControls/" + tb.Name + ".xaml", UriKind.Relative)) as UserControl;

        //    // NavigateTo(tb.Name);

        //}


        BindBreadCrumbs();
    }


    public static void addUserControl(UserControl uc, String title)
    {

        showUserControl(uc);
        BreadCrumbsAdd(title);
        NavigationList.Add(uc);
    }

    public static void updateMenu(UserControl uc, String title)
    {
        NavigationList = new List<UserControl>();
        listBreadCrumbs.Clear();
        addUserControl(uc, title);
       
    }

    static void showUserControl(UserControl uc)
    {
        var main = App.Current.MainWindow as MainWindow;
        var pGird = main.ParentGrid.Children[0] as UcMainMenu;
        pGird.GridMain.Content = uc;
    }

    public static void NavigateTo(string userControl) {

        var main = App.Current.MainWindow as MainWindow;
        var pGird = main.ParentGrid.Children[0] as UcMainMenu;

        //if (pGird.GridMain.CanGoBack)
        //{
        //    Page pg = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;
        //    JournalEntry firstItem = null;
        //    foreach (JournalEntry item in pGird.GridMain.BackStack)
        //    {
        //        firstItem = item;
        //    }
        //    /*
        //    foreach (var i in pGird.GridMain.BackStack)
        //    {


        //        var name = pGird.GridMain.NavigationService.Source;
                
        //        //var controlName=  i.CustomContentState.JournalEntryName ;


        //        // var userControl = i as UserControl;
        //        //if (uc.Name == controlName) {
        //        //    pGird.GridMain.Navigate(uc);
        //        //}


        //    }*/
        //}

    }

    // Add Page Name in Bread Crumbs
    public static void BreadCrumbsAdd(string strName)
    {
        try
        {
            listBreadCrumbs.Add(strName);
            BindBreadCrumbs();
        }
        catch (Exception ex)
        {
        }
    }

    // Remove Page Name from Bread Crumbs
    public static void BreadCrumbsRemove(string strName)
    {
        try
        {
            listBreadCrumbs.Reverse();
            listBreadCrumbs.Remove(strName);
            listBreadCrumbs.Reverse();
            BindBreadCrumbs();
        }
        catch (Exception ex)
        {
        }
    }
}
