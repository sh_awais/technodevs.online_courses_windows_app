﻿using Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using TechEd360.UserControls;

namespace TechEd360
{
    public static class RestAPI
    {
       // static JavaScriptSerializer serializer = new JavaScriptSerializer();

        static RestClient client = new RestClient("http://course_online.technodevs.com/api/");


       // Action<T> success, Action<string> failure
        public static void ApiRequest<T>(string uri, Dictionary<string, string> param, ApiCallback<T> callback) where T : new()
            {
            RestRequest request = new RestRequest(uri, Method.POST) { RequestFormat = RestSharp.DataFormat.Json };

            //adding parameteres
            if (param != null) { 
            foreach (KeyValuePair<string, string> entry in param)
            {

                request.AddParameter(entry.Key, entry.Value);
            }

            }

            client.ExecuteAsync<T>(request, response => {
                if (response.ErrorException == null)
                {
                    Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        callback.success(response.Data);
                    });
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        if(response.ErrorMessage.Contains("The remote name could not be resolved: 'course_online.technodevs.com'"))
                        {
                            callback.failure("Please check your internet connection");
                        }
                        else { 
                        callback.failure(response.ErrorMessage);
                            }

                    });
                }

            });

        }

        public interface ApiCallback<T> {
            void success(T result);
            void failure(string message);
        }

        // Login 

        public static void Login(string email, string password, ApiCallback<LoginResponse> callback)
        {
            string uri = "login?";
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("email", email);
            param.Add("password", password);
            ApiRequest<LoginResponse>(uri, param, callback);
          
        }
       
        // Courses to load on Download Screen 
        public static void GetDownloadedCourses(ApiCallback<CourseList> callback) {
            //RestRequest request = new RestRequest(uri, Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            //client.ExecuteAsync<CourseList>(request, response =>
            //{
            //    DownloadImages(response.Data);
            //    CallbackCourseList(response.Data);
            //});
            string uri = "courses?auth_token=" + UserData.Default.auth_token;
            ApiRequest<CourseList>(uri, null,callback);

        }
        // Courses Files on Download 
        public static void GetCourseFiles(int courseID, ApiCallback<Cours> callback) {
            //RestRequest request = new RestRequest(uri,Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            // client.ExecuteAsync<Cours>(request, response =>
            //{
            //    CallbackCourse(response.Data);

            //});

            string uri = "courses/" + courseID + "/download_course?auth_token=" + UserData.Default.auth_token;
            ApiRequest<Cours>(uri, null, callback);
        }

        public static void DownloadAllPurchased(string uri, Action<CourseList> CallbackDownloadAllPurchased) {
            RestRequest request = new RestRequest(uri, Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            client.ExecuteAsync<CourseList>(request, response =>
            {
                CallbackDownloadAllPurchased(response.Data);
            }
            
            );

        }

        public static void GetDownLoadUrl(string uri,Action<DownloadURL> callback) {
            RestRequest request = new RestRequest(uri+ "?auth_token="+UserData.Default.auth_token, Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
              client.ExecuteAsync<DownloadURL>(request,response => {

                callback(response.Data);
            });
          
         }

        //Downloading Course Images
        public static void DownloadImages(CourseList courseList)
        {
            if (courseList.courses == null)
            {
                return;
            }

            for (int i = 0; i < courseList.courses.Count; i++)
            {

                string CourseImagePath = Path.Combine(
                                                    @UserData.Default.email,
                                                    courseList.courses[i].category,
                                                    courseList.courses[i].title);

                if (!Directory.Exists(CourseImagePath))
                {
                    Directory.CreateDirectory(CourseImagePath);
                }

                string url = courseList.courses[i].image_url.Replace("widthxheight", "500x500");
                string fileName = Path.GetFileName(url.Replace(@"+", string.Empty));
                int index = fileName.IndexOf("?");
                if (index > 0)
                    fileName = fileName.Substring(0, index);
                bool fileExist = File.Exists(CourseImagePath + "\\" + fileName);

                if (!fileExist)
                {
                    try
                    {
                        var client = new RestClient(url);
                        var request = new RestRequest("", Method.GET);
                        //request.AddHeader("Content-Type", "application/octet-stream");
                        byte[] response = client.DownloadData(request);

                        File.WriteAllBytes(CourseImagePath + "\\" + fileName, response);

                    }
                    catch (Exception ex)
                    {

                        //MessageBox.Show("Something went wrong", "Error");
                    }
                }


            }

        }
        // Update Password 

        public static void UpdatePassword( string CurrentPassword,  string NewPassword, Action<LoginResponse> callback)
        {
            RestRequest request = new RestRequest("user/update", Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            request.AddParameter("auth_token", UserData.Default.auth_token, ParameterType.GetOrPost);
            request.AddParameter("user[current_password]", CurrentPassword, ParameterType.GetOrPost);
            request.AddParameter("user[password]", NewPassword, ParameterType.GetOrPost);
            client.ExecuteAsync<LoginResponse>(request, response => {
                callback(response.Data);

            });

        }

        // Courses to load for Courses Secreen
        public static void GetNonPurchasedCourses(ApiCallback<CourseList> callback)
        {
            string uri = "courses/non-purchased?auth_token=" + UserData.Default.auth_token;
            ApiRequest<CourseList>(uri, null, callback);
        }

        // Download Complete API
        public static void DownloadComplete(string fileID, Action<Response> CallbackDownloadComplete) {

            string url = "course-file/update/" + fileID + "?auth_token=" + UserData.Default.auth_token;
            RestRequest request = new RestRequest(url, Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            client.ExecuteAsync<Response>(request, response=>{

                CallbackDownloadComplete(response.Data);
            });

        }

    }
}
