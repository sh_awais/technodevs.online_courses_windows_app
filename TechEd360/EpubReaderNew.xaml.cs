﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Xml.Linq;

namespace TechEd360
{
    /// <summary>
    /// Interaction logic for EpubReaderNew.xaml
    /// </summary>
    public partial class EpubReaderNew : UserControl
    {
        private string _tempPath;
        private string _baseMenuXmlDiretory;
        private List<string> _menuItems;
        private int _currentPage;
        private string _fileUrl = "";

        public EpubReaderNew(string fileUrl)
        {
            this._fileUrl = fileUrl;
            InitializeComponent();
            _menuItems = new List<string>();
            NextButton.Visibility = Visibility.Hidden;
            PreviousButton.Visibility = Visibility.Hidden;
        }

        private void EpubViewerNew_Loaded(object sender, RoutedEventArgs e)
        {
            Global.clearFolder(UserData.Default.email + "\\epubs\\");
            Directory.CreateDirectory(UserData.Default.email + "\\epubs\\");
         //   Directory.CreateDirectory(UserData.Default.email + "\\epubs\\images");
            string epubPath = UserData.Default.email + "\\epubs\\";

            String fileNameWithExtention = Path.GetFileName(_fileUrl);
            String fileName = Path.GetFileNameWithoutExtension(_fileUrl);

            //if (!Directory.Exists("epubs"))
            //{
            //    Directory.CreateDirectory("epubs");
            //}

            File.Copy( _fileUrl, Path.Combine(epubPath, fileName + ".zip"), true);
            _tempPath = Path.Combine(epubPath, fileName);

            string ZipPath = Path.Combine(epubPath, fileName + ".zip");
            string ExtractPath = Path.Combine(epubPath, fileName);

            //FileUtility.UnZIPFiles(ZipPath, ExtractPath);

            ZipFile.ExtractToDirectory(ZipPath, ExtractPath);

            //using (ZipArchive archive = ZipFile.Open(ZipPath, ZipArchiveMode.Update))
            //{
            //    archive.ExtractToDirectory(ExtractPath);
            //}


            var containerReader = XDocument.Load(ConvertToMemmoryStream(Path.Combine(epubPath, fileName, "META-INF", "container.xml")));

            var baseMenuXmlPath = containerReader.Root.Descendants(containerReader.Root.GetDefaultNamespace() + "rootfile").First().Attribute("full-path").Value;
            XDocument menuReader = XDocument.Load(Path.Combine(_tempPath, baseMenuXmlPath));
            _baseMenuXmlDiretory = Path.GetDirectoryName(baseMenuXmlPath);
            var menuItemsIds = menuReader.Root.Element(menuReader.Root.GetDefaultNamespace() + "spine").Descendants().Select(x => x.Attribute("idref").Value).ToList();
            _menuItems = menuReader.Root.Element(menuReader.Root.GetDefaultNamespace() + "manifest").Descendants().Where(mn => menuItemsIds.Contains(mn.Attribute("id").Value)).Select(mn => mn.Attribute("href").Value).ToList();
            _currentPage = 0;
            string uri = GetPath(0);
            epubDisplay.Navigate(uri);
            NextButton.Visibility = Visibility.Visible;

        }

        public MemoryStream ConvertToMemmoryStream(string fillPath)
        {
            var xml = File.ReadAllText(fillPath);
            byte[] encodedString = Encoding.UTF8.GetBytes(xml);

            // Put the byte array into a stream and rewind it to the beginning
            MemoryStream ms = new MemoryStream(encodedString);
            ms.Flush();
            ms.Position = 0;

            return ms;
        }

        public string GetPath(int index)
        {
            return String.Format("file:///{0}", Path.GetFullPath(Path.Combine(_tempPath, _baseMenuXmlDiretory, _menuItems[index])));
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentPage < _menuItems.Count - 1)
            {
                _currentPage++;
            }
            else
            {
                NextButton.Visibility = Visibility.Hidden;
            }
            if (_currentPage == _menuItems.Count - 1)
            {
                NextButton.Visibility = Visibility.Hidden;
            }
            if (_currentPage > 0)
            {

                PreviousButton.Visibility = Visibility.Visible;
            }
            string uri = GetPath(_currentPage);
            epubDisplay.Navigate(uri);
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentPage >= 1)
            {
                _currentPage--;
            }
            else
            {

                PreviousButton.Visibility = Visibility.Hidden;
            }
            if (_currentPage == 1)
            {
                PreviousButton.Visibility = Visibility.Hidden;
            }
            if (_currentPage <= _menuItems.Count - 1)
            {
                NextButton.Visibility = Visibility.Visible;
            }
            string uri = GetPath(_currentPage);
            epubDisplay.Navigate(uri);
        }
    }
}
