﻿using Microsoft.Win32;
using Newtonsoft.Json;
using SGet;
using System;
using System.IO;
using System.Windows;
using System.Xml.Linq;
using TechEd360.UserControls;

namespace TechEd360
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public UcDownloadManager ucDownloadManager;
         
        public MainWindow()
        {
            InitializeComponent();
            Style = (Style)FindResource(typeof(Window));
            // In case of computer shutdown or restart, save current list of downloads to an XML file
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            ucDownloadManager = new UcDownloadManager();
            if (!(UserData.Default.auth_token.Length > 0))
            {
                ParentGrid.Children.Add(new Login());
            }
            else {
                showMainContent();
            }
        }
        public void showMainContent(){
            ParentGrid.Children.Clear();
            ParentGrid.Children.Add(new UcMainMenu());

        }

        public void logout()
        {
            UserData.Default.Reset();
            File.Delete(Global.courseListFile);
            ParentGrid.Children.Clear();
            Window_Loaded(null, null);// to show login form again
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDownloadsToXml();
            Global.clearFolder(UserData.Default.email + "\\epubs\\");
            string str;

            if (!File.Exists(Global.courseListFile))
            {
                return;
            }

            File.Delete(Global.courseListFile);
            str = JsonConvert.SerializeObject(Global.courseList);
            File.WriteAllText(Global.courseListFile, str);


        }

        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            SaveDownloadsToXml();
        }

        private void PauseAllDownloads()
        {
            if (ucDownloadManager.lbDownloadFiles.Items.Count > 0)
            {
                foreach (WebDownloadClient download in SGet.DownloadManager.Instance.DownloadsList)
                {
                    download.Pause();
                }
            }
        }

        private void SaveDownloadsToXml()
        {
            if (SGet.DownloadManager.Instance.TotalDownloads > 0)
            {
                // Pause downloads
                PauseAllDownloads();

                XElement root = new XElement("downloads");

                foreach (WebDownloadClient download in SGet.DownloadManager.Instance.DownloadsList)
                {
                    string username = String.Empty;
                    string password = String.Empty;
                    if (download.ServerLogin != null)
                    {
                        username = download.ServerLogin.UserName;
                        password = download.ServerLogin.Password;
                    }

                    XElement xdl = new XElement("download",
                                        new XElement("file_name", download.FileName),
                                        new XElement("url", download.Url.ToString()),
                                        new XElement("username", username),
                                        new XElement("password", password),
                                        new XElement("temp_path", download.TempDownloadPath),
                                        new XElement("file_size", download.FileSize),
                                        new XElement("downloaded_size", download.DownloadedSize),
                                        new XElement("status", download.Status.ToString()),
                                        new XElement("status_text", download.StatusText),
                                        new XElement("total_time", download.TotalElapsedTime.ToString()),
                                        new XElement("added_on", download.AddedOn.ToString()),
                                        new XElement("completed_on", download.CompletedOn.ToString()),
                                        new XElement("supports_resume", download.SupportsRange.ToString()),
                                        new XElement("has_error", download.HasError.ToString()),
                                        new XElement("open_file", download.OpenFileOnCompletion.ToString()),
                                        new XElement("temp_created", download.TempFileCreated.ToString()),
                                        new XElement("is_batch", download.IsBatch.ToString()),
                                        new XElement("url_checked", download.BatchUrlChecked.ToString()));
                                        new XElement("Percent_string", download.PercentString.ToString());
                    root.Add(xdl);
                }

                XDocument xd = new XDocument();
                xd.Add(root);
                // Save downloads to XML file
                xd.Save(UserData.Default.email + @"\Downloads.xml");
            }
        }

        public void ToggleBusyIndicator() {

            if (BusyIndicator.Visibility == Visibility.Collapsed) {

                ParentGrid.IsEnabled = false;
                BusyIndicator.Visibility = Visibility.Visible;
            }
            else {

                ParentGrid.IsEnabled = true;
                BusyIndicator.Visibility = Visibility.Collapsed;
            }
        }
    }
}


