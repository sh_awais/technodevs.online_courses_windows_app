﻿
using Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;


namespace TechEd360.Repo
{
    public static class DownloadsRepo
    {

        public static CourseList courseList = new CourseList();
        public static int SelectedCourse { get; set; }

        //Todo change to AsycCall
        //public static Cours GetCurrentCourseFiles(int id)
        //{
        //    return RestAPI.GetCourseFiles("courses/" + id + "/download_course?auth_token=" + UserData.Default.auth_token, Method.POST);
        //}

        //Send Course Title
        public static string GetCurrentCourseTitle()
        {
            return courseList.courses[SelectedCourse].title;
        }

        public static void DownloadImages()
        {
            if (courseList.courses.Count == 0)
            {
                return;
            }

            for (int i = 0; i < courseList.courses.Count; i++)
            {

                string CourseImagePath = Path.Combine(
                                                    @UserData.Default.email,
                                                    courseList.courses[i].category,
                                                    courseList.courses[i].title);

                if (!Directory.Exists(CourseImagePath))
                {
                    Directory.CreateDirectory(CourseImagePath);
                }

                string url = courseList.courses[i].image_url.Replace("widthxheight", "500x500");
                string fileName = Path.GetFileName(url.Replace(@"+", string.Empty));
                int index = fileName.IndexOf("?");
                if (index > 0)
                    fileName = fileName.Substring(0, index);
                bool fileExist = File.Exists(CourseImagePath + "\\" + fileName);

                if (!fileExist)
                {
                    try
                    {
                        var client = new RestClient(url);
                        var request = new RestRequest("", Method.GET);
                        //request.AddHeader("Content-Type", "application/octet-stream");
                        byte[] response = client.DownloadData(request);

                        File.WriteAllBytes(CourseImagePath +"\\"+ fileName, response);

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Something went wrong","Error");
                    }
                }


            }

        }

        public static BitmapImage GetCurrentCourseImage(Cours cours)
        {
            string s = Path.Combine(
                            Directory.GetCurrentDirectory(),
                            UserData.Default.email,
                            cours.category,
                           cours.title);
            string ImageFile = Directory.GetFiles(s, "*.png", SearchOption.TopDirectoryOnly).FirstOrDefault().ToString();
            return new BitmapImage(new Uri(ImageFile));
        }

        public static string getDownloadPath(CoureFile file) {
            // file path will be UserEmail + Category + Course + FileType
            string filePath = UserData.Default.email;

            filePath += "\\" + courseList.courses[SelectedCourse].category;
            filePath += "\\" + courseList.courses[SelectedCourse].title;
            filePath += "\\" + Global.GetFolderName(file.content_type);

            return filePath;

        }


        //public static CoureFile GetUpdatedFile(int fileID,int CourseID)
        //{
        //    CoureFile file =  courseList.courses.Where(c=> c.id == CourseID).FirstOrDefault().course_files.Where(f => f.id == fileID).FirstOrDefault();

        //    string filePath = UserData.Default.email;
        //    filePath += "\\" + courseList.courses[SelectedCourse].category;
        //    filePath += "\\" + courseList.courses[SelectedCourse].title;
        //    filePath += "\\" + Global.GetFolderName(file.content_type);
        //    //filePath += "\\" + courseList.courses[SelectedCourse].course_files.Where(f => f.id == fileID).FirstOrDefault().file_name;
        //    file.file_path = filePath;

        //    file.download_url = RestAPI.GetDownLoadUrl(file.download_url + "?auth_token=" + UserData.Default.auth_token);

        //    return file;
        //}




    }
}
