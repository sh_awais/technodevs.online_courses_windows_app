﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using TechEd360.ViewModels;

namespace TechEd360.Repo
{
    public static class HomeRepo
    {

        //   public static CourseList courseList = new CourseList();
        public static List<VMCategories> CategoriesList ;
        public static int SelectedCourse;
        public static void UpdatecourseList()
        {
            Global.clearFolder(UserData.Default.email + "\\epubs\\");
            CategoriesList = new List<VMCategories>();
            DirectoryInfo UserDirectory = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\" + UserData.Default.email);

            List<string> tempfiles = new List<string>();
            try {

                 tempfiles = Directory.GetFiles(UserDirectory.FullName, "*.*", SearchOption.AllDirectories).Where(name => !name.EndsWith(".png")).ToList();

            }

            catch (Exception ex) {

                Console.Write("Error : " + ex.Message.ToString());
                return;
            }

            var files = tempfiles;

            if (!Directory.Exists(UserDirectory.ToString())|| files.Count() == 0 )
                    
                  { return; }

                List<DirectoryInfo> Categories = UserDirectory.GetDirectories().ToList();

                foreach (DirectoryInfo d in Categories)
                {
                    VMCategories CourseCategory = new VMCategories();
                    CourseCategory.CategoryName = Path.GetFileName(d.FullName);


                    //Skip Category
                    var foundFiles = Directory.GetFiles(d.FullName, "*.*", SearchOption.AllDirectories).Where(name => !name.EndsWith(".png")).ToList();
                     if (!Directory.Exists(UserDirectory.ToString()) || foundFiles.Count() == 0)
                    { continue; }

                    List<DirectoryInfo> Courses = d.GetDirectories().ToList();
                    //Course Data course Image Audio Videos And Ebooks
                    foreach (DirectoryInfo coursDirectory in Courses)
                    {

                        
                        //Skip Course
                        var filesinCourse = Directory.GetFiles(coursDirectory.FullName, "*.*", SearchOption.AllDirectories).Where(name => !name.EndsWith(".png")).ToList();
                        if (!Directory.Exists(UserDirectory.ToString()) || filesinCourse.Count() == 0)
                        { continue; }

                        VMCourse SingleCourse = new VMCourse();

                        SingleCourse.CourseName = Path.GetFileName(coursDirectory.FullName);
                       SingleCourse.Category = CourseCategory.CategoryName;

                        //  For course Image
                    string ImageFile = Directory.GetFiles(coursDirectory.FullName, "*.png", SearchOption.TopDirectoryOnly).FirstOrDefault().ToString();
                        SingleCourse.CourseImage = new BitmapImage(new Uri(ImageFile));

                        // For Audio Video And Ebook
                        List<string> Video = null;
                        List<string> Audio = null;
                        List<string> Ebook = null;

                        if (Directory.Exists(coursDirectory.FullName + "\\Video"))
                        {
                            Video = Directory.GetFiles(coursDirectory.FullName + "\\Video").ToList();
                        }

                        if (Directory.Exists(coursDirectory.FullName + "\\Audio"))
                        {
                            Audio = Directory.GetFiles(coursDirectory.FullName + "\\Audio").ToList();
                        }

                        if (Directory.Exists(coursDirectory.FullName + "\\Ebook"))
                        {
                            Ebook = Directory.GetFiles(coursDirectory.FullName + "\\Ebook").ToList();

                        }

                    
                        SingleCourse.Video = Video;
                        SingleCourse.Audio = Audio;
                        SingleCourse.Ebook = Ebook;

                        CourseCategory.Courses.Add(SingleCourse);
                    }


                    CategoriesList.Add(CourseCategory);

              

            }

        }

    }
}
