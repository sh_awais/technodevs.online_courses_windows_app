﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace TechEd360.ViewModels
{
    public class VMCourseContent
    {
       public string SerialNo { get; set; }
       public string Title { get; set; }
       public BitmapImage Image  { get; set; }
       public string FileName { get; set; }

    }
}
