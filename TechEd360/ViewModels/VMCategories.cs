﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace TechEd360.ViewModels
{
    public class VMCategories
    {
        public VMCategories()
            {
                Courses = new List<VMCourse>();
            }
            public string CategoryName { get; set; }
        
        public List<VMCourse> Courses { get; set; }
    }


    public class VMCourse
    {
        public BitmapImage CourseImage { get; set; }
        public string Category { get; set; }
        public string CourseName { get; set; }
        public List<string> Video { get; set; }
        public List<string> Audio { get; set; }
        public List<string> Ebook { get; set; }

    }

    
}
