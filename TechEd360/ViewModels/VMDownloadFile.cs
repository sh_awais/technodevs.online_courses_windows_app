﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TechEd360.ViewModels
{
    class VMDownloadFile
    {

        public VMDownloadFile(){

            isButtonEnable =true;
        ProgressVisibility = Visibility.Hidden;
        DownloadProgress = 0;
        }
        public string SerialNo { get; set; }
        public string Title { get; set; }
        public string DownloadUrl { get; set; }
        public int DownloadProgress { get; set; }
        public int FileId { get; set; }

        public Visibility ProgressVisibility { get; set; }
        public bool isButtonEnable { get; set; }
        public CoureFile courseFile { get; set; }
    }
}
