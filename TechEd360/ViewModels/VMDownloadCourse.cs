﻿using System.Windows.Media.Imaging;

namespace TechEd360.ViewModels
{
    // using for both downloads and courses screen
    class VMDownloadCourse

        
    {
        public VMDownloadCourse() {
            isButtonEnable = true;
        }

        public int id { get; set; }
        public BitmapImage Image { get; set; }
        public string SerialNo { get; set; }
        public string Title { get; set; } 
        public bool isButtonEnable { get; set; }


    }
}
