﻿using Models;
using SGet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TechEd360.Repo;
using TechEd360.ViewModels;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcHome.xaml
    /// </summary>
    public partial class UcDownloads : UserControl , RestAPI.ApiCallback<CourseList>, RestAPI.ApiCallback<Cours>
    {
        private static List<VMDownloadCourse> CustomCoursList;
        private static int SerialNo;//for Serial no
        MainWindow mainWindows = App.Current.MainWindow as MainWindow;


        public UcDownloads()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            //DownloadsRepo.UpdatecourseList();
            lbCourses.SelectedItem = null;
        
            mainWindows.ToggleBusyIndicator();
            RestAPI.GetDownloadedCourses( this);
            
        }
        
        private void lbCourses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListBox)sender).SelectedItems.Count == 0) { return; }
            var v = (VMDownloadCourse)((ListBox)sender).SelectedItems[0];

            List<Cours> courses = DownloadsRepo.courseList.courses.ToList();
            Cours cours = courses.Where(c => c.id == v.id).FirstOrDefault();

            Module.addUserControl(new UcDownloadContent(cours), cours.title);

           // Global.Navigate(new UcDownloadContent(cours));

        }

        private void BtnCouseDownload_Click(object sender, RoutedEventArgs e)
        {
            var CurrentButton = sender as Button;

            var viewModel = CurrentButton.DataContext as VMDownloadCourse;
            CurrentButton.IsEnabled = false;

            RestAPI.GetCourseFiles(viewModel.id, this);

        }

        //private void DownloadCourse(Cours obj)
        //{

        //    //  mainWindows.BusyIndicator.Visibility = Visibility.Visible;

        //    Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {

        //        foreach (CoureFile file in obj.course_files)
        //        {

        //            file.categoryTitle = obj.category;
        //            file.courseTitle = obj.title;

        //            // Check if the file already exists then don't add it
        //            if (File.Exists(file.GetLocalPath()) || File.Exists(file.GetTempPath())) { continue; }


        //            // Check if file is already downloading then dont add it
        //            //if (SGet.DownloadManager.Instance.DownloadsList.Count > 0)
        //            //{
        //            //    WebDownloadClient fileCheck = SGet.DownloadManager.Instance.DownloadsList.Where(d => d.FileName == file.file_name).FirstOrDefault();
        //            //    if (fileCheck == null)
        //            //    {
        //            //        continue;
        //            //    }
        //            //}


        //            StartDownloadingFile(file);

        //        }

        //    });

              

        //    //Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {
        //    //    mainWindows.BusyIndicator.Visibility = Visibility.Collapsed;
        //    //});
          
        //}

        private void StartDownloadingFile(CoureFile file)
        {
            WebDownloadClient download = new WebDownloadClient(file);

            // Register WebDownloadClient events
            download.DownloadProgressChanged += download.DownloadProgressChangedHandler;
            download.DownloadCompleted += download.DownloadCompletedHandler;

            download.StatusChanged += mainWindows.ucDownloadManager.StatusChangedHandler;
            download.DownloadCompleted += mainWindows.ucDownloadManager.DownloadCompletedHandler;

        }



        //DownlaodManager Button
        private void BtnOpenDownloadManager_Click(object sender, RoutedEventArgs e)
        {
                Module.addUserControl(mainWindows.ucDownloadManager, "Download Manager");
      
        
        }

        private void BtnDownloadAll_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.IsEnabled = false;

            for (int i = 0; i < CustomCoursList.Count; i++)
            {

                var v = CustomCoursList[i];
                v.isButtonEnable = false;


            }
            lbCourses.Items.Refresh();

            string uri = "courses/download_all_purchased?auth_token=" + UserData.Default.auth_token;
            RestAPI.DownloadAllPurchased(uri, CallbackDownloadAllPurchased);


        }

        private void CallbackDownloadAllPurchased(CourseList obj)
        {
            Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {
             //   BusyIndicator.Visibility = Visibility.Visible;
                foreach (Cours cours in obj.courses)
                {
                    DownloadCourse(cours);
                }
               // BusyIndicator.Visibility = Visibility.Collapsed;
            });
               

        }

        void RestAPI.ApiCallback<CourseList>.success(CourseList orignalCourseList)
        {
            if (orignalCourseList.courses == null)
            {
                mainWindows.ToggleBusyIndicator();
                MessageBox.Show("Session Expire");
                mainWindows.logout();
                return;
            }


            RestAPI.DownloadImages(orignalCourseList);
            DownloadsRepo.courseList = orignalCourseList;
            SerialNo = 0;
            CustomCoursList = new List<VMDownloadCourse>();

            for (int i = 0; i < orignalCourseList.courses.Count(); i++)
            {
                SerialNo += 1;
                VMDownloadCourse TempCourse = new VMDownloadCourse();
                TempCourse.SerialNo = Global.PadZero(SerialNo);
                TempCourse.Title = orignalCourseList.courses[i].title;

                string s = Path.Combine(Directory.GetCurrentDirectory(), UserData.Default.email, orignalCourseList.courses[i].category, orignalCourseList.courses[i].title);
                string ImageFile = Directory.GetFiles(s, "*.png", SearchOption.TopDirectoryOnly).FirstOrDefault().ToString();
                TempCourse.Image = new BitmapImage(new Uri(ImageFile));
                TempCourse.id = orignalCourseList.courses[i].id;
                CustomCoursList.Add(TempCourse);

            }



            lbCourses.ItemsSource = CustomCoursList;
            mainWindows.ToggleBusyIndicator();
      

        }


       void DownloadCourse( Cours obj) {

            foreach (CoureFile file in obj.course_files)
            {

                file.categoryTitle = obj.category;
                file.courseTitle = obj.title;

                // Check if the file already exists then don't add it
                if (File.Exists(file.GetLocalPath()) || File.Exists(file.GetTempPath())) { continue; }


                // Check if file is already downloading then dont add it
                //if (SGet.DownloadManager.Instance.DownloadsList.Count > 0)
                //{
                //    WebDownloadClient fileCheck = SGet.DownloadManager.Instance.DownloadsList.Where(d => d.FileName == file.file_name).FirstOrDefault();
                //    if (fileCheck == null)
                //    {
                //        continue;
                //    }
                //}


                StartDownloadingFile(file);

            }

        }

    void RestAPI.ApiCallback<CourseList>.failure(string message)
        {
            mainWindows.ToggleBusyIndicator();
            MessageBox.Show(message, "Error");
        }

        void RestAPI.ApiCallback<Cours>.success(Cours obj)
        {
            if (obj == null) {
                return;
            }

            DownloadCourse(obj);
        }

        void RestAPI.ApiCallback<Cours>.failure(string message)
        {
            MessageBox.Show(message, "Error");
        }
    }
}
