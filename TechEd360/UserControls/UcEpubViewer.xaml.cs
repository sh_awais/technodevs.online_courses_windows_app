﻿//using EpubSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using VersOne.Epub;

namespace TechEd360.UserControls
    
{
    /// <summary>
    /// Interaction logic for UcEpubViewer.xaml
    /// </summary>
    /// 
   
    public partial class UcEpubViewer : UserControl
       
    {
        private string _fileUrl = "";
        public UcEpubViewer(string fileUrl)
        {
            this._fileUrl = fileUrl;
            InitializeComponent();
            EpubViewer.Navigate(new Uri("about:blank"));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try

            {

                Global.clearFolder(UserData.Default.email + "\\epubs\\");
                Directory.CreateDirectory(UserData.Default.email + "\\epubs\\");
                Directory.CreateDirectory(UserData.Default.email + "\\epubs\\images");


                //FileStream fs = File.Open(_fileUrl, FileMode.Open);
                EpubBook epubBook = EpubReader.ReadBook(_fileUrl);

                // CONTENT

                // Book's content (HTML files, stlylesheets, images, fonts, etc.)
                EpubContent bookContent = epubBook.Content;
                // All XHTML files in the book (file name is the key)
                Dictionary<string, EpubTextContentFile> htmlFiles = bookContent.Html;

                // Entire HTML content of the book
                string htmlContent = "";
                foreach (EpubTextContentFile htmlFile in htmlFiles.Values)
                {
                    htmlContent = htmlContent + htmlFile.Content;
                }

                File.WriteAllText(UserData.Default.email + "\\epubs\\TempEpub.html", htmlContent);

                // All CSS files in the book (file name is the key)
                Dictionary<string, EpubTextContentFile> cssFiles = bookContent.Css;

                // All CSS content in the book
                foreach (EpubTextContentFile cssFile in cssFiles.Values)
                {
                  
                    File.WriteAllText(UserData.Default.email + "\\epubs\\" + cssFile.FileName, cssFile.Content);
                }


                // IMAGES

                // All images in the book (file name is the key)
                Dictionary<string, EpubByteContentFile> images = bookContent.Images;

                foreach (EpubByteContentFile image in images.Values)
                {
                    File.WriteAllBytes(UserData.Default.email  + "\\epubs\\" + image.FileName, image.Content);
                    
                }


                string str = Directory.GetCurrentDirectory() + "\\" +UserData.Default.email + "\\epubs\\TempEpub.html";
                Uri uri = new Uri(@str, UriKind.Absolute);
                EpubViewer.Navigate(uri);
             
            }

            catch (Exception ex)
            {

                MessageBox.Show("Error" + ex.Message.ToString());
            }

        }


        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
           Global.clearFolder(UserData.Default.email + "\\epubs\\");
        }
    }
}
