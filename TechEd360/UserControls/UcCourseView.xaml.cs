﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TechEd360.ViewModels;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcCourseView.xaml
    /// </summary>
    public partial class UcCourseView : UserControl
    {
        private VMCourse _vMCourse ;

        public UcCourseView(VMCourse vMCourse)
        {
            _vMCourse = vMCourse;
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            courseImage.Source = _vMCourse.CourseImage;
            tbCourseTitle.Text = _vMCourse.CourseName;
            
        }

        private void btnVideo_Click(object sender, RoutedEventArgs e)
        {
            if (_vMCourse.Video == null) { MessageBox.Show("No Files Available Against this Category");  return; }
            ViewContent("Video");
        }
        private void btnAudio_Click(object sender, RoutedEventArgs e)
        {
            if (_vMCourse.Audio == null) { MessageBox.Show("No Files Available Against this Category"); return; }
            ViewContent("Audio");
        }
        private void btnEbook_Click(object sender, RoutedEventArgs e)
        {
            if (_vMCourse.Ebook == null) { MessageBox.Show("No Files Available Against this Category"); return; }
            ViewContent("Ebook");
        }
        private void ViewContent(string contentType)
        {
            Module.addUserControl(new UcCourseContent(_vMCourse, contentType),contentType);

            //Global.Navigate(new UcCourseContent(_vMCourse, contentType));

        }
  
    }
}
