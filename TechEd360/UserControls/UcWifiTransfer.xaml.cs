﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Unosquare.Labs.EmbedIO;
using Unosquare.Labs.EmbedIO.Constants;
using Unosquare.Labs.EmbedIO.Modules;
namespace TechEd360.UserControls
{

    public class MyServer : WebServer
    {
       public MyServer(string urlPrefix, Unosquare.Labs.EmbedIO.Constants.RoutingStrategy strategy): base(urlPrefix, strategy)
        {
        }
        public void Dispose()
        {
            Dispose(true);
        }
    }

    /// <summary>
    /// Interaction logic for UcWifiTransfer.xaml
    /// </summary>
    public partial class UcWifiTransfer : UserControl
    {

      static private MyServer server;
        static private WebApiModule module;

        public UcWifiTransfer()
        {
            InitializeComponent();
        }

        private void BtnStartServer_Click(object sender, RoutedEventArgs e)
        {
            ToggleServer();

        }

        private void ToggleServer()
        {
            if (btnStartServer.Content.Equals("Start Server"))
            {
                    if (server != null)
                {
                    stopServer();
                }
                    server = new MyServer("http://localhost:8080/", Unosquare.Labs.EmbedIO.Constants.RoutingStrategy.Regex);
                     module = new WebApiModule();
                    module.RegisterController<WifiController>();
                 server.RegisterModule(module);
                //server.Module<WebApiModule>().RegisterController<WifiController>();
                server.RunAsync();
               
                // Opening link in web browser
                System.Diagnostics.Process.Start("http://localhost:8080/wifiTransfer");
                Global.isServerStarted = true;
                ToggleInfo();
            }
            else {
                stopServer();
                Global.isServerStarted = false;
                ToggleInfo();

            }
        }

        private void stopServer()
        {
            server.UnregisterModule(module.GetType());
            module = null;
            server.Dispose();
            server = null;
        }

        private void ToggleInfo() {

            switch (Global.isServerStarted) {

                case true:
                    imgServer.Source = new BitmapImage(new Uri(@"pack://application:,,,/TechEd360;component/Resources/serverOn.png", UriKind.Absolute));
                    lblPort.Visibility = Visibility.Visible;
                    tbhyperlink.Visibility = Visibility.Visible;
                    btnStartServer.Content = "Stop Server";
                    break;

                default:
                    imgServer.Source = new BitmapImage(new Uri(@"pack://application:,,,/TechEd360;component/Resources/serverOff.png", UriKind.Absolute));
                    lblPort.Visibility = Visibility.Hidden;
                    tbhyperlink.Visibility = Visibility.Hidden;
                    btnStartServer.Content = "Start Server";
                    break;

            }

            //Changing image and button content and show info
         
        }



        private void Tbhyperlink_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://localhost:8080/wifitransfer");
        }

        private void UcWifiTransfer_Loaded(object sender, RoutedEventArgs e)
        {
            ToggleInfo();
        }
    }
}
