﻿using Models;
using SGet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using TechEd360.DownloadManager;
using TechEd360.Repo;
using TechEd360.ViewModels;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcDownloadContent.xaml
    /// </summary>
    public partial class UcDownloadContent : UserControl, RestAPI.ApiCallback<Cours>
    {
     
        private static int SerialNo;//for Serial no
        private static List<VMDownloadFile> CustomFiles;
        private  Cours CourseWithFiles;
        List<Grid> MyRenderedGrid = new List<Grid>();
        Button CurrentButton;
        ProgressBar CurrentProgressBar;
        MainWindow mainWindows = App.Current.MainWindow as MainWindow;

        private Cours _course;
        public UcDownloadContent(Cours cours)
        {
            this._course = cours;
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbCourseTitle.Text = _course.title; // Fill Title
            courseImage.Source = DownloadsRepo.GetCurrentCourseImage(_course); // Fill Image



         

            mainWindows.ToggleBusyIndicator();
            RestAPI.GetCourseFiles(_course.id, this);


            //CourseWithFiles = DownloadsRepo.GetCurrentCourseFiles(_course.id);
            //if (CourseWithFiles == null)
            //{
                
            //}
         
        }

        private void CourseCallBack(Cours obj)
        {
            Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {
                mainWindows.ToggleBusyIndicator();
                try {

                    FillData(obj);
                    CourseWithFiles = obj;
                }

                catch (Exception ex) {
                    Debug.Write("Error" + ex.ToString());
                    MessageBox.Show("Error while getting files!","Error"); return;
                }

            });
        }

        //MyDownloaderInterface
        public void ProcessUpdated(int progress, CoureFile file)
        {
            Debug.Write("Progress Value : " + progress);
            this.Dispatcher.Invoke(() =>
                    {
                       CurrentProgressBar.Value = progress;
                        if (progress == 100)
                            { 
                                CurrentProgressBar.Visibility = Visibility.Hidden;
                                CurrentButton.Visibility = Visibility.Visible;
                            }
                    });
        }

        private void FillData(Cours OrignalFiles)
        {
            SerialNo = 0;
            CustomFiles = new List<VMDownloadFile>();

            foreach (CoureFile f in OrignalFiles.course_files) {
                SerialNo += 1;
                VMDownloadFile TempFile = new VMDownloadFile();
                TempFile.SerialNo = Global.PadZero(SerialNo);
                TempFile.Title = f.file_name;
                TempFile.DownloadUrl = f.download_url;
                TempFile.DownloadProgress = 0;
                TempFile.FileId = f.id;
                TempFile.courseFile = f;
                CustomFiles.Add(TempFile);
            
            }

            lbFiles.ItemsSource = CustomFiles;
            
        }
        private void btnDownloadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                CurrentButton = sender as Button;

                var v = CurrentButton.DataContext as VMDownloadFile;


             
               // CoureFile file =  DownloadsRepo.GetUpdatedFile((int)CurrentButton.Tag,_course.id);
                Grid CurrentGrid = CurrentButton.Parent as Grid;

                //Updating Category and Course Title
                v.courseFile.categoryTitle = _course.category;
                v.courseFile.courseTitle = _course.title;
                StartDownloadingFile(v.courseFile);

                v.isButtonEnable = false;
                lbFiles.Items.Refresh();

                //CurrentButton.Visibility = Visibility.Hidden;
               
                //CurrentProgressBar = CurrentGrid.Children[2] as ProgressBar;
                //CurrentProgressBar.Visibility = Visibility.Visible;
                //CurrentProgressBar.Value = 0;
            }

            catch (Exception ex)
            {
                Debug.Write("Error :" + ex.ToString());
                MessageBox.Show("File is Already Downloaded");
            }
        }

        private void BtnDownloadAllFiles_Click(object sender, RoutedEventArgs e)
        {
            
              for(int i = 0; i<CourseWithFiles.course_files.Count;i++)
                {

                CourseWithFiles.course_files[i].categoryTitle = _course.category;
                CourseWithFiles.course_files[i].courseTitle = _course.title;

                StartDownloadingFile(CourseWithFiles.course_files[i]);

                var v = CustomFiles[i];
                v.isButtonEnable = false;
               
               
            }
            lbFiles.Items.Refresh();

        }

        private void StartDownloadingFile(CoureFile file) {

            WebDownloadClient download = new WebDownloadClient(file);

            // Register WebDownloadClient events
            download.DownloadProgressChanged += download.DownloadProgressChangedHandler;
            download.DownloadCompleted += download.DownloadCompletedHandler;

            download.StatusChanged += mainWindows.ucDownloadManager.StatusChangedHandler;
            download.DownloadCompleted += mainWindows.ucDownloadManager.DownloadCompletedHandler;




            // Check if the file already exists
            //if (File.Exists(file.GetLocalPath()))
            //{


            //    string message = "There is already a file with the same name, do you want to overwrite it? "
            //                   + "If not, please change the file name or download folder.";
            //    MessageBoxResult result = MessageBox.Show(message,
            //                                            "File Name Conflict: " + file.GetLocalPath(),
            //                                              MessageBoxButton.YesNo, MessageBoxImage.Warning);

            //    if (result == MessageBoxResult.Yes)
            //    {
            //        File.Delete(file.GetLocalPath());
            //    }
            //    else
            //        return;
            //}

    
        }

        private void BtnOpenDownloadManager_Click(object sender, RoutedEventArgs e)
        {

            Module.addUserControl(mainWindows.ucDownloadManager, "Download Manager");
          //  Global.Navigate(mainWindows.ucDownloadManager);

        }

        void RestAPI.ApiCallback<Cours>.success(Cours result)
        {
            mainWindows.ToggleBusyIndicator();
            try
            {

                FillData(result);
                CourseWithFiles = result;
            }

            catch (Exception ex)
            {
                Debug.Write("Error" + ex.ToString());
                MessageBox.Show("Error while getting files!", "Error"); return;
            }

        }

        void RestAPI.ApiCallback<Cours>.failure(string message)
        {
            mainWindows.ToggleBusyIndicator();
            MessageBox.Show(message, "Error");
        }



        //private CoureFile updateDownloadUrl(CoureFile file)
        //{

        //    if (Path.HasExtension(file.download_url)) return file;

        //    string url = RestAPI.GetDownLoadUrl(file.download_url + "?auth_token=" + UserData.Default.auth_token);

        //    for (int i = 0; i < DownloadsRepo.courseList.courses.Count; i++)
        //    {
        //        for (int j = 0; j < DownloadsRepo.courseList.courses[i].course_files.Count; j++)
        //        {
        //            if (DownloadsRepo.courseList.courses[i].course_files[j].id == file.id)
        //            {
        //                DownloadsRepo.courseList.courses[i].course_files[j].download_url = url;
        //                return DownloadsRepo.courseList.courses[i].course_files[j];
        //            }
        //        }
        //    }

        //    return null;

        //}


    }
}
