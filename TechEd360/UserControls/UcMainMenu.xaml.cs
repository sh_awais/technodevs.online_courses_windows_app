﻿using Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using TechEd360.Repo;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcMainMenu.xaml
    /// </summary>
    public partial class UcMainMenu : UserControl
    {
       
        MainWindow mainWindows = App.Current.MainWindow as MainWindow;
        public UcMainMenu()
        {
            InitializeComponent();
            Style = (Style)FindResource(typeof(UserControl));
           
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
           
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
           

        }
        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            
                ButtonCloseMenu.Visibility = Visibility.Collapsed;
                ButtonOpenMenu.Visibility = Visibility.Visible;
           
        }

        private int previousSelectedMenu =-1;

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tbWelcome.Visibility = Visibility.Collapsed;
            lblBreadCrumb.Visibility = Visibility.Visible;
            int selectedItem = ListViewMenu.SelectedIndex;

            string str = ((ListViewItem)((ListView)sender).SelectedItem).Name;
            if (previousSelectedMenu == selectedItem) { return; }
      
            switch (str)
            {
            
                case "Home":
                    previousSelectedMenu = selectedItem;
                    Module.updateMenu(new UcHome(), "Home");
                   break;
                case "Downloads":
                    DownloadsRepo.courseList = null;
                    previousSelectedMenu = selectedItem;
                    Module.updateMenu(new UcDownloads(), "Downloads");
                    break;
                //case "DownloadManager":
                   
                //    GridMain.Navigate(mainWindows.ucDownloadManager);
                //    break;
                case "WifiTransfer":
                    previousSelectedMenu = selectedItem;
                    Module.updateMenu(new UcWifiTransfer(), "Wifi Transfer");
                    break;
                case "UserInfo":
                    previousSelectedMenu = selectedItem;
                    Module.updateMenu(new UcUserInfo(), "User Info");
                    break;

                case "Courses":
                    previousSelectedMenu = selectedItem;
                    Module.updateMenu(new UcCourses(), "Courses");
                    break;

                case "Logout":
                    
                    logout();
                    break;
                default:
                    break;
            }
        }

        private void logout() {

            if (MessageBox.Show("Are you sure you want to logout?", "Logout", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                ListViewMenu.SelectedIndex = previousSelectedMenu; // For Logout

                return;
            }
            else
            {
                mainWindows.logout();
            }

        }
    
        
        private void ShowUserData()
        {

            lblUserName.Content = UserData.Default.firstName.Trim() + " " + UserData.Default.lastName.Trim();

        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            tbWelcome.Visibility = Visibility.Collapsed;
            lblBreadCrumb.Visibility = Visibility.Hidden;
            ShowUserData();
            ListViewMenu.SelectedIndex = 0;  // todo uncomments
          
        }



   
    }
}
