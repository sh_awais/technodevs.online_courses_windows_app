﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Models;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcChangePassword.xaml
    /// </summary>
    public partial class UcChangePassword : UserControl
    {

        MainWindow mainWindows = App.Current.MainWindow as MainWindow;
        public UcChangePassword()
        {
            InitializeComponent();
        }

        private void BtnUpdatePassword_Click(object sender, RoutedEventArgs e)
        {
            string CurrentPassword = tbCurrentPassword.Password.ToString().Trim();
            string NewPassword = tbNewPassword.Password.ToString().Trim();
            string ConfirmPassword = tbConfirmPassword.Password.ToString().Trim();

            if (CurrentPassword=="" || NewPassword == "" || ConfirmPassword == "" )
            {
                MessageBox.Show("Please fill all fields");
                return;
            }



            if (NewPassword != ConfirmPassword) {
                MessageBox.Show("New Password and Confirm Password must be matched");
                return;
            }

            mainWindows.ToggleBusyIndicator();
            RestAPI.UpdatePassword(CurrentPassword, NewPassword, UpdatePasswordResponse);

        }

        private void UpdatePasswordResponse(LoginResponse obj)
        {
            Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {
                mainWindows.ToggleBusyIndicator();
                if (obj.success)
                {
                    clearValues();
                    MessageBox.Show("Password Updated !");
                    return;
                }
                else
                    clearValues();
                MessageBox.Show("Error While Updating Password ");
                return;


            });

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            clearValues();
        }

        private void clearValues() {
            tbCurrentPassword.Password = "";
            tbNewPassword.Password = "";
            tbConfirmPassword.Password = "";
        }
    }
}
