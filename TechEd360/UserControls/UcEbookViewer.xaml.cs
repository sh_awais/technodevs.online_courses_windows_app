﻿
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcEbookViewer.xaml
    /// </summary>
    public partial class UcEbookViewer : UserControl
    {
        private string fileUrl = "";
        public UcEbookViewer(string fileName)
        {

            
            this.fileUrl = fileName;
            InitializeComponent();
            //pdfViewer.Navigate(new Uri("about:blank"));


        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(fileUrl))
            {
               
                MessageBox.Show("File Not Found", "Error");
                return;
            }
            try
            {
                
                pdfViewer.Address = fileUrl;

            }

            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message.ToString());
            }
        }
    }
}
