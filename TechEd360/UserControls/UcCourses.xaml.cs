﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using Models;
using TechEd360.Repo;
using TechEd360.ViewModels;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcCourses.xaml
    /// </summary>
    /// 
  

    public partial class UcCourses : UserControl,RestAPI.ApiCallback<CourseList>
  
    {
        private static List<VMDownloadCourse> CustomCoursList;
        private static int SerialNo;//for Serial no
        MainWindow mainWindows = App.Current.MainWindow as MainWindow;

        public UcCourses()
        {
            InitializeComponent();
        }

        private void NonPurchasedCourses_Loaded(object sender, RoutedEventArgs e)
        {
         
            lbCourses.SelectedItem = null;

            mainWindows.ToggleBusyIndicator();
            RestAPI.GetNonPurchasedCourses( this);
          
        }

        private void CallbackCourseList(CourseList orignalCourseList)
        {

            Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate {

               
            });



        }

        private void BtnBuyNow_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LbCourses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        void RestAPI.ApiCallback<CourseList>.success(CourseList orignalCourseList)
        {
            if (orignalCourseList.courses == null)
            {
                mainWindows.ToggleBusyIndicator();
                MessageBox.Show("Session Expire");
                mainWindows.logout();
                return;
            }


            RestAPI.DownloadImages(orignalCourseList);
            DownloadsRepo.courseList = orignalCourseList;
            SerialNo = 0;
            CustomCoursList = new List<VMDownloadCourse>();

            for (int i = 0; i < orignalCourseList.courses.Count(); i++)
            {
                SerialNo += 1;
                VMDownloadCourse TempCourse = new VMDownloadCourse();
                TempCourse.SerialNo = Global.PadZero(SerialNo);
                TempCourse.Title = orignalCourseList.courses[i].title;

                string s = Path.Combine(Directory.GetCurrentDirectory(), UserData.Default.email, orignalCourseList.courses[i].category, orignalCourseList.courses[i].title);
                string ImageFile = Directory.GetFiles(s, "*.png", SearchOption.TopDirectoryOnly).FirstOrDefault().ToString();
                TempCourse.Image = new BitmapImage(new Uri(ImageFile));
                TempCourse.id = orignalCourseList.courses[i].id;
                CustomCoursList.Add(TempCourse);

            }



            lbCourses.ItemsSource = CustomCoursList;
            mainWindows.ToggleBusyIndicator();
        }

        void RestAPI.ApiCallback<CourseList>.failure(string message)
        {
            mainWindows.ToggleBusyIndicator();
            MessageBox.Show(message, "Error");
        }
    }
}
