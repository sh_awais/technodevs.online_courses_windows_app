﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UserInfo.xaml
    /// </summary>
    public partial class UcUserInfo : UserControl
    {
        public UcUserInfo()
        {

            InitializeComponent();
            this.tbFirstName.Text = UserData.Default.firstName;
            this.tbLastName.Text = UserData.Default.lastName;
            this.tbCompany.Text = UserData.Default.company;

        }

       
        private void BtnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            Module.addUserControl(new UcChangePassword(), "Change Password");

           // Global.Navigate( new UcChangePassword());

        }

        private void BtnEditProfile_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
