﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TechEd360.ViewModels;


namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcCourseContent.xaml
    /// </summary>
    public partial class UcCourseContent : UserControl
    {
        private static List<VMCourseContent> CourseContent;
        List<string> files;
        private string _contentType="";
        

        private static int SerialNo;//for Serial no
        private VMCourse _vMCourse;
        public UcCourseContent( VMCourse vMCourse, string contentType)
        {
            _vMCourse = vMCourse;
            // this.CoursePosition = Global.SelectedCourse;
            this._contentType = contentType;
            InitializeComponent();
        }

        private void loadCourseContent(List<string> files)
        {
            SerialNo = 0;
          CourseContent =  new List<VMCourseContent>();

            for (int i = 0; i < files.Count(); i++)
            {
                
                    SerialNo += 1;
                    VMCourseContent content = new VMCourseContent();
                    content.SerialNo = Global.PadZero(SerialNo);
                    content.Title = Path.GetFileNameWithoutExtension(files[i]);
                    content.Image = getContentImage(_contentType);
                    content.FileName = files[i];
                    CourseContent.Add(content);
            
            }
       
            lbCourseContent.ItemsSource = CourseContent;
           // Module.BreadCrumbsAdd(_contentType + "|UcCourseContent");
        }

        private List<string> GetFiles(string contentType)
        {
           files = new List<string>();

         switch(_contentType)
            {

                case  "Video":
                    files = _vMCourse.Video;
                    break;

                case "Audio":
                    files = _vMCourse.Audio;
                    break;

                default:
                    files = _vMCourse.Ebook;
                    break;
            }

            return files;

           
        }

        private BitmapImage getContentImage(string contentType)
        {

            switch (contentType)
            {

                case "Video":
                    return new BitmapImage(new Uri(@"pack://application:,,,/TechEd360;component/Resources/video_hover_icon.png", UriKind.Absolute));
                case "Audio":
                    return new BitmapImage(new Uri(@"pack://application:,,,/TechEd360;component/Resources/audio_hover_icon.png", UriKind.Absolute));
                default:
                    return new BitmapImage(new Uri(@"pack://application:,,,/TechEd360;component/Resources/e-book_hover_icon.png", UriKind.Absolute));

            }

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            lbCourseContent.SelectedItem = null;
            List<string> files = GetFiles(_contentType);
            if (files == null) {
                MessageBox.Show("No Files Found!"); return;
            }

           
            loadCourseContent(files);
            setCourseContent();

            
        }

        private void setCourseContent()
        {
            switch (_contentType) {

                case "Video":
                    tbContentTitle.Text = "Course Video Content";
                    break;
                case "Audio":
                    tbContentTitle.Text = "Course Audio Content";
                    break;
                default:
                    tbContentTitle.Text = "Course Ebook Content";
                    break;
            }
        }

        private void lbCourseContent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbCourseContent.SelectedItems.Count == 0) { return; }

            int pos = lbCourseContent.SelectedIndex;
            var v = (VMCourseContent)(lbCourseContent.SelectedItems[0]);
            playContent(v.FileName,pos);
           

        }

       
        private void playContent(string fileName, int pos) {

            switch (_contentType) { 
            
                case "Video":
                   
                  Global.Navigate(new UcMediaPlayer(files, pos));
                    break;
                case "Audio":
                       Global.Navigate(new UcAudioPlayer(_vMCourse, pos));
                    break;         
                default:
                    string fileExtention = Path.GetExtension(fileName);
                    if (fileExtention == ".epub")
                    {
                        //Global.Navigate(new UcEpubViewer(fileName));
                        Global.Navigate(new EpubReaderNew(fileName));
                    }
                    else { 

                    Global.Navigate(new UcEbookViewer(fileName));
                   
                    }

                    break;

            }
        }
       
    }
}
