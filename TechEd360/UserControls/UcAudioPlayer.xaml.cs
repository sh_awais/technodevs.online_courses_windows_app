﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using TechEd360.ViewModels;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcAudioPlayer.xaml
    /// </summary>
    public partial class UcAudioPlayer : UserControl
    {
        private string fileUrl = "";
        private VMCourse _vMCourse;
        private List<string> _playList;
        private int _SelectedItem;
        private List<string> _fileNames = new List<string>();
        public UcAudioPlayer(VMCourse vMCourse, int SelectedItem)
        {
            this._vMCourse = vMCourse;
            _playList = vMCourse.Audio;
            _SelectedItem = SelectedItem;
            InitializeComponent();
            //Player.uiMode = "mini";
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            courseImage.Source = _vMCourse.CourseImage;
            CourseName.Text = _vMCourse.CourseName;
            Category.Text = "Category: " + _vMCourse.Category;

            //Extacting Names to show in listbox
            foreach (string file in _playList)
            {
                _fileNames.Add(Path.GetFileName(file));
            }

            //Creating play list and assigning it to Player
            WMPLib.IWMPPlaylist playlist = Player.playlistCollection.newPlaylist("myplaylist");
            WMPLib.IWMPMedia media;
            foreach (string file in _playList)
            {
                media = Player.newMedia(file);
                playlist.appendItem(media);
            }

            Player.currentPlaylist = playlist;
            lbPlayList.ItemsSource = _fileNames;
            lbPlayList.SelectedIndex = _SelectedItem;
        }

        private void LbPlayList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //fileUrl = _playList[lbPlayList.SelectedIndex];
            //Player.URL = fileUrl;

            // Get the media item  position in the current playlist.
            WMPLib.IWMPMedia media = Player.currentPlaylist.get_Item(lbPlayList.SelectedIndex);

            // Play the media item.
            Player.Ctlcontrols.playItem(media);

        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (Player != null) {
                Player.Dispose();
                Player = null;
                
            }
            
        }




        //Select Play list next item on auto play of next track of playlist
        public void Player_CurrentItemChange(System.Object sender, AxWMPLib._WMPOCXEvents_CurrentItemChangeEvent e)
        {

            int LoopCount = _playList.Count - 1;
            for (int i = 0; i <= LoopCount; i++)
            {
                if (Player.currentMedia.sourceURL == _playList[i])
                {
                    lbPlayList.SelectedIndex = i;
                    break;
                }
            }
        }
    }
}
