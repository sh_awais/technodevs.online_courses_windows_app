﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Unosquare.Labs.EmbedIO;
using Unosquare.Labs.EmbedIO.Constants;
using Unosquare.Labs.EmbedIO.Modules;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using AntsCode.Util;
using System.Windows.Media.Imaging;

namespace TechEd360.UserControls
{ 

    public class WifiController : WebApiController
    {
        // You need to add a default constructor where the first argument
        // is an IHttpContext
        public WifiController(IHttpContext context)
            : base(context)
        {
        }

        // You need to include the WebApiHandler attribute to each method
        // where you want to export an endpoint. The method should return
        // bool or Task<bool>.
        [WebApiHandler(HttpVerbs.Get, "/wifiTransfer")]
        public async Task<bool> GetPersonById(int id)
        {

           
            string html = File.ReadAllText("FileUpload.html");
            // This is fake call to a Repository
            //  var person = await PeopleRepository.GetById(id);
            return await this.HtmlResponseAsync(html);

        }

        //Making Copy of file 
        /*
        public void copyFile()
        {
            var data1 = await this.ParseJsonAsync<FileAddress>();

            string str = data1.file_address;
            var fileName = Path.GetFileName(str);

            string destination = Global.GetUploadPath() + "\\" + Global.GetDirectoryName(Path.GetExtension(fileName));

            // Create Upload Dicrectory and its image
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);

                File.WriteAllBytes(img);
            }



            System.IO.File.Copy(data1.file_address, destination + "\\" + fileName, true);
        }

            catch (Exception ex)
            {
                Debug.Write("Error : " + ex.Message.ToString());

            }
}*/

        [WebApiHandler(HttpVerbs.Post, "/PostFile")]
        public async Task<bool> PostFile()
        {
           string fileName =  this.QueryString("fileName");

            string UploadPath = Global.GetUploadPath() + "\\";

            // Create Upload Dicrectory and its image
            if (!Directory.Exists(UploadPath))
            {
                Directory.CreateDirectory(UploadPath);

                //Copying image from debug folder to uploads folder
                System.IO.File.Copy("uploads.png", UploadPath + "uploads.png", true);

            }

            string destination = UploadPath + Global.GetDirectoryName(Path.GetExtension(fileName));

            // Create Upload Dicrectory and its image
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
              
            }


            Upload(this.Request.InputStream, destination + "\\" + fileName);
            
           // var body = await this.RequestFormDataDictionaryAsync();
            //var body = await this.RequestBodyAsync();

            //var data = await this.RequestFormDataDictionaryAsync();
            //  var fileName = data["fileName"];

            //try
            //{
            //    var keys = data.Keys.ToArray();

            //    //empty data 
            //    if (keys.Length == 0)
            //    {
            //        return true;
            //    }

            //    //get result
            //    var key = keys[0];
            //    // var keyfile = keys[1];



            //    var result = data[key] as Dictionary<string, object>; ;

            //    //get file
            //    var file = result["file"];
            //    // var fileName1= data["fileName"];
            //    var fileName2 = result["fileName"];

            //    // Debug.Write("File Name : " + fileName);
            //    // Perform an operation with the data
            //    // await SaveData(data);
            //}

            //catch (Exception ex) {
            //    Debug.Write("Error : " + ex.Message.ToString());

            //}
            return true;
        }

        public void Upload(Stream stream,string destination)
        {
            MultipartParser parser = new MultipartParser(stream);

            try
            {
                if (parser.Success)
                {
                    // Save the file
                   // string destination = @"D:\123.png";
                    File.WriteAllBytes(destination, parser.FileContents);
                    //SaveFile(parser.Filename, parser.ContentType, parser.FileContents);
                }
                else
                {

                }
            }catch(Exception ex)
            {
                Debug.Write("Error" + ex.Message.ToString());
            }
        }
        // You can override the default headers and add custom headers to each API Response.
        public override void SetDefaultHeaders() => this.NoCache();


    }


    public class FileAddress
    {

        public string file_address { get; set; }

    }
}




