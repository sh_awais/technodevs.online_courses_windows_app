﻿using Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;


namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl, RestAPI.ApiCallback<LoginResponse>
    {
        MainWindow mainWindow = App.Current.MainWindow as MainWindow;
        public Login()
        {
            InitializeComponent();
           // Style = (Style)FindResource(typeof(Window));
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            string email = txtEmail.Text.ToString().Trim();
            string password = txtPassword.Password.ToString().Trim();

            if (email.Length < 1 || password.Length < 1)
            {
                MessageBox.Show("Enter Valid Credentials", "Error");
                return;
            }

            mainWindow.ToggleBusyIndicator();
            RestAPI.Login(email, password, this);

            //          RestAPI.Login("login?" , email, password, callback);

        }


        void RestAPI.ApiCallback<LoginResponse>.success(LoginResponse result)
        {
            mainWindow.ToggleBusyIndicator();
            if (result.success == false)
            {
                MessageBox.Show("Error with your login or password", "Error");
            }
            else
            {
                //Saving User Data and loading main menu
                SaveUserDate(result);
                mainWindow.ParentGrid.Children.Remove(mainWindow.ParentGrid.Children[0]);
                mainWindow.showMainContent(); // showing main menu
            }
        }

        void RestAPI.ApiCallback<LoginResponse>.failure(string message)
        {
            mainWindow.ToggleBusyIndicator();
            MessageBox.Show(message, "Error");
        }



        private void SaveUserDate(LoginResponse loginResponse)
        {
            UserData.Default.auth_token = loginResponse.auth_token;
            UserData.Default.firstName = loginResponse.first_name ;
            UserData.Default.lastName = loginResponse.last_name;
            UserData.Default.email = loginResponse.email;
            UserData.Default.company = loginResponse.company;
            UserData.Default.Save();

            //Creating Directory of User
            System.IO.Directory.CreateDirectory(loginResponse.email);
        }

        private void OpenHyperlink(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri);
        }
       
    }
}
