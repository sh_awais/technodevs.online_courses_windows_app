﻿using SGet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using System.IO;
using TechEd360.Properties;
using Hardcodet.Wpf.TaskbarNotification;
using System.Threading;
using Models;

namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcDownloadManager.xaml
    /// </summary>
    public partial class UcDownloadManager : UserControl
    {
        private List<string> propertyNames;
        private List<string> propertyValues;
        private List<PropertyModel> propertiesList;

        public UcDownloadManager()
        {
            InitializeComponent();

            //Fill Download Limit Combo Box
            FillDownloadLimit();


            // Bind DownloadsList to downloadsGrid

            lbDownloadFiles.ItemsSource = SGet.DownloadManager.Instance.DownloadsList;
           // downloadsGrid.ItemsSource = SGet.DownloadManager.Instance.DownloadsList;

            LoadDownloadsFromXml();
            propertyNames = new List<string>();
            propertyNames.Add("URL");
            propertyNames.Add("Supports Resume");
            propertyNames.Add("File Type");
            propertyNames.Add("Download Folder");
            propertyNames.Add("Average Speed");
            propertyNames.Add("Total Time");
            propertyValues = new List<string>();
            propertiesList = new List<PropertyModel>();
           // SetEmptyPropertiesGrid();
           
            // Load downloads from the XML file
         

            if (SGet.DownloadManager.Instance.TotalDownloads == 0)
            {
               // EnableMenuItems(false);

                // Clean temporary files in the download directory if no downloads were loaded
                if (Directory.Exists(Settings.Default.DownloadLocation))
                {
                    DirectoryInfo downloadLocation = new DirectoryInfo(Settings.Default.DownloadLocation);
                    foreach (FileInfo file in downloadLocation.GetFiles())
                    {
                        if (file.FullName.EndsWith(".tmp"))
                            file.Delete();
                    }
                }
            }


            //To do change in speed and limit of download
             
            //Settings.Default.EnableSpeedLimit = false; // false
           // Settings.Default.SpeedLimit = 50; //200
        }

        private void FillDownloadLimit()
        {
            for (int i = 1; i <= 5; i++) {
                cbDownloadLimit.Items.Add(i);            
            }
            cbDownloadLimit.SelectedItem = Settings.Default.MaxDownloads;

        }

        #region Methods

        private void PauseAllDownloads()
        {
            if (downloadsGrid.Items.Count > 0)
            {
                foreach (WebDownloadClient download in SGet.DownloadManager.Instance.DownloadsList)
                {
                    download.Pause();
                }
            }
        }

      

        private void LoadDownloadsFromXml()
        {
            try
            {
                if (File.Exists(UserData.Default.email + @"\Downloads.xml"))
                {
                    // Load downloads from XML file
                    XElement downloads = XElement.Load(UserData.Default.email + @"\Downloads.xml");
                    if (downloads.HasElements)
                    {
                        IEnumerable<XElement> downloadsList =
                            from el in downloads.Elements()
                            select el;
                        foreach (XElement download in downloadsList)
                        {
                            // Create WebDownloadClient object based on XML data
                            WebDownloadClient downloadClient = new WebDownloadClient(download.Element("url").Value);

                            downloadClient.FileName = download.Element("file_name").Value;

                            downloadClient.DownloadProgressChanged += downloadClient.DownloadProgressChangedHandler;
                            downloadClient.DownloadCompleted += downloadClient.DownloadCompletedHandler;
                           // downloadClient.PropertyChanged += PropertyChangedHandler;
                            downloadClient.StatusChanged += StatusChangedHandler;
                            downloadClient.DownloadCompleted += DownloadCompletedHandler;
                            

                            downloadClient.TempDownloadPath = download.Element("temp_path").Value;
                            downloadClient.FileSize = Convert.ToInt64(download.Element("file_size").Value);
                            downloadClient.DownloadedSize = Convert.ToInt64(download.Element("downloaded_size").Value);

                           SGet.DownloadManager.Instance.DownloadsList.Add(downloadClient);

                            if (download.Element("status").Value == "Completed")
                            {
                                downloadClient.Status = SGet.DownloadStatus.Completed;
                            }
                            else
                            {
                                downloadClient.Status = SGet.DownloadStatus.Paused;
                            }

                            downloadClient.StatusText = download.Element("status_text").Value;

                            downloadClient.ElapsedTime = TimeSpan.Parse(download.Element("total_time").Value);
                            downloadClient.AddedOn = DateTime.Parse(download.Element("added_on").Value);
                            downloadClient.CompletedOn = DateTime.Parse(download.Element("completed_on").Value);

                            downloadClient.SupportsRange = Boolean.Parse(download.Element("supports_resume").Value);
                            downloadClient.HasError = Boolean.Parse(download.Element("has_error").Value);
                            downloadClient.OpenFileOnCompletion = Boolean.Parse(download.Element("open_file").Value);
                            downloadClient.TempFileCreated = Boolean.Parse(download.Element("temp_created").Value);
                            downloadClient.IsBatch = Boolean.Parse(download.Element("is_batch").Value);
                            downloadClient.BatchUrlChecked = Boolean.Parse(download.Element("url_checked").Value);
                           // downloadClient.PercentString = "0.0";
                            


                            if (downloadClient.Status != SGet.DownloadStatus.Completed && !downloadClient.HasError)
                            {
                                downloadClient.Start();
                            }
                        }

                        // Create empty XML file
                       XElement root = new XElement("downloads");
                        XDocument xd = new XDocument();
                        xd.Add(root);
                        xd.Save(UserData.Default.email + @"\Downloads.xml");
                    }
                }
            }
            catch (Exception)
            {
               MessageBox.Show("There was an error while loading the download list.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion


        public void StatusChangedHandler(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(new EventHandler(StatusChanged), sender, e);
        }

        private void StatusChanged(object sender, EventArgs e)
        {
            // Start the first download in the queue, if it exists
            WebDownloadClient dl = (WebDownloadClient)sender;
            if (dl.Status == SGet.DownloadStatus.Paused || dl.Status == SGet.DownloadStatus.Completed
                || dl.Status == SGet.DownloadStatus.Deleted || dl.HasError)
            {
                foreach (WebDownloadClient d in SGet.DownloadManager.Instance.DownloadsList)
                {
                    if (d.Status == SGet.DownloadStatus.Queued)
                    {
                        d.Start();
                        break;
                    }
                }
            }

            foreach (WebDownloadClient d in SGet.DownloadManager.Instance.DownloadsList)
            {
                if (d.Status == SGet.DownloadStatus.Downloading)
                {
                    d.SpeedLimitChanged = true;
                }
            }

            int active = SGet.DownloadManager.Instance.ActiveDownloads;
            int completed = SGet.DownloadManager.Instance.CompletedDownloads;

            if (dl.Status == SGet.DownloadStatus.Error) {

            }

        }

        public void DownloadCompletedHandler(object sender, EventArgs e)
        {
           
                WebDownloadClient download = (WebDownloadClient)sender;

            if (download.Status == SGet.DownloadStatus.Completed)
            {
              //Calling Download Complete API
                RestAPI.DownloadComplete(download.courseFile.id.ToString(), CallbackDownloadComplete);

                // Notification on download complete
                //string title = "Download Completed";
                //string text = download.FileName + " has finished downloading.";
                //if (Settings.Default.ShowBalloonNotification)
                //{
                //   XNotifyIcon.ShowBalloonTip(title, text, BalloonIcon.Info); 
                //}

                download.Status = SGet.DownloadStatus.Deleted;

                Application.Current.Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    SGet.DownloadManager.Instance.DownloadsList.Remove(download);
                });

            }
          

        }

        private void CallbackDownloadComplete(Response obj)
        {
            
            //dont need to do anything on failure
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            var CurrentButton = sender as Button;
            var selectedDownload = CurrentButton.DataContext as WebDownloadClient;

            selectedDownload.Status = SGet.DownloadStatus.Deleted;
            if (File.Exists(selectedDownload.TempDownloadPath))
                File.Delete(selectedDownload.TempDownloadPath);
            SGet.DownloadManager.Instance.DownloadsList.Remove(selectedDownload);

            lbDownloadFiles.Items.Refresh();

        }

        private void BtnPlayPause_Click(object sender, RoutedEventArgs e)
        {

          var   CurrentButton = sender as Button;
            var selectedDownloads = CurrentButton.DataContext as WebDownloadClient;

            Label ButtonLabel = (Label)((DockPanel)CurrentButton.Content).Children[1];

            if (ButtonLabel.Content.ToString() == "Pause")
            {
                ButtonLabel.Content = "Play";
                var ButtonIcon = (MaterialDesignThemes.Wpf.PackIcon)((DockPanel)CurrentButton.Content).Children[0];
                ButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Play;
                selectedDownloads.Pause();
            }
            else if (ButtonLabel.Content.ToString() == "Retry")
            {

                ButtonLabel.Content = "Play";
                var ButtonIcon = (MaterialDesignThemes.Wpf.PackIcon)((DockPanel)CurrentButton.Content).Children[0];
                ButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Play;
                selectedDownloads.RestartAsync();
            }
            else {

                ButtonLabel.Content = "Pause";
                var ButtonIcon = (MaterialDesignThemes.Wpf.PackIcon)((DockPanel)CurrentButton.Content).Children[0];
                ButtonIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Pause;
                selectedDownloads.Start();
            }

            
            

        }

        private void CbDownloadLimit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           int NewMaxDownloads = (int)cbDownloadLimit.SelectedItem;

           
            //return if limit is same
            if (NewMaxDownloads == Settings.Default.MaxDownloads) { return; }

            //update speed limit
            Settings.Default.MaxDownloads = NewMaxDownloads;
            Settings.Default.Save();

            //return if download list is empty
            if (SGet.DownloadManager.Instance.TotalDownloads == 0) { return; }


            /*
                    //starting new downloades if new max downloade limit is greateer
                    foreach (WebDownloadClient d in SGet.DownloadManager.Instance.DownloadsList)
                    {
                        if (SGet.DownloadManager.Instance.ActiveDownloads < NewMaxDownloads)
                        {
                            if (d.Status == SGet.DownloadStatus.Queued)
                            {
                                d.Start();
                            }
                        }
                    }

                    //starting new downloades if new max downloade limit is greateer
                    for (int i = SGet.DownloadManager.Instance.TotalDownloads - 1; i >= 0; i--)
                    {
                        if (SGet.DownloadManager.Instance.ActiveDownloads > NewMaxDownloads)
                        {
                            if (SGet.DownloadManager.Instance.DownloadsList[i].Status == SGet.DownloadStatus.Waiting
                                || SGet.DownloadManager.Instance.DownloadsList[i].Status == SGet.DownloadStatus.Downloading)
                            {
                                SGet.DownloadManager.Instance.DownloadsList[i].Status = SGet.DownloadStatus.Queued;
                            }
                        }
                    }*/

            foreach (WebDownloadClient d in SGet.DownloadManager.Instance.DownloadsList)
            {
                if (SGet.DownloadManager.Instance.ActiveDownloads < Settings.Default.MaxDownloads)
                {
                    if (d.Status == SGet.DownloadStatus.Queued)
                    {
                        d.Start();
                    }
                }
            }
            for (int i = SGet.DownloadManager.Instance.TotalDownloads - 1; i >= 0; i--)
            {
                if (SGet.DownloadManager.Instance.ActiveDownloads > Settings.Default.MaxDownloads)
                {
                    if (SGet.DownloadManager.Instance.DownloadsList[i].Status == SGet.DownloadStatus.Waiting
                        || SGet.DownloadManager.Instance.DownloadsList[i].Status == SGet.DownloadStatus.Downloading)
                    {
                        SGet.DownloadManager.Instance.DownloadsList[i].Status = SGet.DownloadStatus.Queued;
                    }
                }
            }



        }


    }
}
