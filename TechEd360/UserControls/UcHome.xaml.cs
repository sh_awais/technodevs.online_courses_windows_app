﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using TechEd360.Repo;
using TechEd360.ViewModels;



namespace TechEd360.UserControls
{
    /// <summary>
    /// Interaction logic for UcHome.xaml
    /// </summary>
    public partial class UcHome : UserControl
    {
        MainWindow mainWindow = App.Current.MainWindow as MainWindow;

        public object Helpers { get; private set; }

        // private List<VMCategories> CategoriesList;
        // private  List<Cours> courses;
        public UcHome()
        {
            InitializeComponent();
          
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try {
                


                mainWindow.ToggleBusyIndicator();
                var ucMainMenu = mainWindow.ParentGrid.Children[0] as UcMainMenu;
                ucMainMenu.tbWelcome.Visibility = Visibility.Visible;
                ucMainMenu.lblBreadCrumb.Visibility = Visibility.Hidden;

                HomeRepo.UpdatecourseList();
                lvCategories.ItemsSource = HomeRepo.CategoriesList;
                mainWindow.ToggleBusyIndicator();
            }
            catch (Exception ex) {
                MessageBox.Show("Error While Loading Local Files", "Error");

            }
        }

        private void lbCourse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListBox)sender).SelectedItems.Count == 0) { return; }
            var v = (VMCourse)((ListBox)sender).SelectedItems[0];

            var ucMainMenu = mainWindow.ParentGrid.Children[0] as UcMainMenu;
            ucMainMenu.tbWelcome.Visibility = Visibility.Collapsed;
            ucMainMenu.lblBreadCrumb.Visibility = Visibility.Visible;

            Module.addUserControl(new UcCourseView(v),v.CourseName);

            // Global.Navigate(new UcCourseView(v));

        }

        private void ScroolRight_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var obj = sender as Button;
            var MainDockPanel = ((DockPanel)((Border)((StackPanel)((DockPanel)obj.Parent).Parent).Parent).Parent).Parent as DockPanel;
            var lbCourses = MainDockPanel.Children[1] as ListBox;

            var scrollViewer = GetDescendantByType(lbCourses, typeof(ScrollViewer)) as ScrollViewer;
            scrollViewer.LineRight();
        }

        public static Visual GetDescendantByType(Visual element, Type type)
        {
            if (element == null)
            {
                return null;
            }
            if (element.GetType() == type)
            {
                return element;
            }
            Visual foundElement = null;
            if (element is FrameworkElement)
            {
                (element as FrameworkElement).ApplyTemplate();
            }
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;
                foundElement = GetDescendantByType(visual, type);
                if (foundElement != null)
                {
                    break;
                }
            }
            return foundElement;
        }

        private void ScroolLeft_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var obj = sender as Button;
            var MainDockPanel = ((DockPanel)((Border)((StackPanel)((DockPanel)obj.Parent).Parent).Parent).Parent).Parent as DockPanel;
            var lbCourses = MainDockPanel.Children[1] as ListBox;
            var scrollViewer = GetDescendantByType(lbCourses, typeof(ScrollViewer)) as ScrollViewer;
            scrollViewer.LineLeft();
       
        }

        private void instScroll_Loaded(object sender, RoutedEventArgs e)
        {
            lvCategories.AddHandler(MouseWheelEvent, new RoutedEventHandler(MyMouseWheelH), true);
        }

        private void MyMouseWheelH(object sender, RoutedEventArgs e)
        {

            MouseWheelEventArgs eargs = (MouseWheelEventArgs)e;

            double x = (double)eargs.Delta;

            double y = instScroll.VerticalOffset;

            instScroll.ScrollToVerticalOffset(y - x);
        }
    }
}