﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using TechEd360.Repo;


namespace TechEd360.DownloadManager
{

    public interface MyDownloaderInterface
    {
         void ProcessUpdated(int progress, CoureFile file);
    
    }
   public class MyDownloader
    {
       public MyDownloaderInterface interfaceListner;
       private static  MyDownloader instance;
       private static readonly object padlock = new object();
       private MyDownloader(){}
       public static MyDownloader Instance
       {
           get
           {
               lock (padlock) { 
               if (instance == null)
                   instance = new MyDownloader();
               return instance;
               }
           }
       }

     private  List<CoureFile> pendingDownlaodedFiles = new List<CoureFile>();
     private  uint currentCount = 0;
     private uint maxCount = 1;
     private CoureFile currentFile;

     private string getDownloadDirectory(CoureFile file)
     {
         string directory = DownloadsRepo.getDownloadPath(file);


         if (!Directory.Exists(directory))
         {
             Directory.CreateDirectory(directory);
         }
         return directory;
     }

     private String getFilePath(CoureFile file)
     {

         string fileName = Path.GetFileName(file.download_url.Replace(@"+", string.Empty));
         int index = fileName.IndexOf("?");
         if (index > 0)
             fileName = fileName.Substring(0, index);
         string localUrl = getDownloadDirectory(file) + "\\" + fileName;
         return localUrl;
     }

     public void Download(CoureFile file)
     {
            //create local path from file download url
            string localUrl = getFilePath(file);
         //return if file already exists
        if (File.Exists(localUrl)) { return; }

        if (currentCount >= maxCount) {
            pendingDownlaodedFiles.Add(file);
            return;
        }

        //mantains downloading file count
        currentCount += 1;
        updatefileStatus(file, DownloadStatus.Downloading,null);
        
         //maintaining refefence of current file
         currentFile = file;
        startDownload(file.download_url, localUrl);
     }


     private void startDownload(string downloadUrl, string localUrl)
        {
            Thread thread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true; 
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                client.DownloadFileAsync(new Uri(downloadUrl), @localUrl);

            });
          
            thread.Start();
        }


        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)             // handle error scenario
            {
                currentFile = null;
                currentCount -= 1;
            }
            
            if (e.Cancelled)  // handle cancelled scenario
            {
                currentFile = null;
                currentCount -= 1;
            }

            //updating file status and local url 
            updatefileStatus(currentFile, DownloadStatus.Downloaded, getFilePath(currentFile));
           
            //clearing old file references
            currentFile = null;
            currentCount -= 1;
            interfaceListner.ProcessUpdated(100, currentFile);
            //downloading next pending file
            startPendingDownloading();
            
         
        }

        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            int percentage = (int)(bytesIn / totalBytes * 100);
       
           // Debug.WriteLine("Downloaded " + e.BytesReceived + " of " + e.TotalBytesToReceive);
            //Debug.WriteLine("Downloaded Progress " + percentage + " %");
            interfaceListner.ProcessUpdated(percentage, currentFile);
        }


        //updating file status and local url
        private void updatefileStatus(CoureFile file, DownloadStatus downloadStatus, string localUrl)
        {
            //directory + "\\" + fileName
            for (int i = 0; i < DownloadsRepo.courseList.courses.Count; i++)
            {
                for (int j = 0; j < Global.courseList.courses[i].course_files.Count; j++)
                {
                    if (Global.courseList.courses[i].course_files[j].id == file.id)
                    {
                        Global.courseList.courses[i].course_files[j].download_status = downloadStatus;
                        //Global.courseList.courses[i].course_files[j].local_url = localUrl;
                        //interfaceListner.ProcessUpdated(100, currentFile);
                    }
                }
            }

        }

       private void startPendingDownloading() {
           if (pendingDownlaodedFiles.Count > 0) {
               CoureFile file = pendingDownlaodedFiles.Where(x => x.download_status == DownloadStatus.NotStarted).First();
                        
               this.Download(file);
           }
           
       }


    }
}
